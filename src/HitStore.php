<?php

namespace Abzudev\Hits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class HitStore extends Model
{
    protected $table = 'hit_store';

    public $timestamps = false;

    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
