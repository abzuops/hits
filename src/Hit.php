<?php

namespace Abzudev\Hits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Hit extends Model
{
    protected $table = 'hits';

    public $timestamps = false;

    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
