<?php

namespace Abzudev\Hits\Traits;

use Abzudev\Hits\Hit;
use Abzudev\Hits\HitStore;

trait Hittable
{
    public function hit()
    {
        $hit = new Hit();
        $hit->model_id = $this->$this->primaryKey;
        $hit->media_type = get_class($this);
        $hit->timestamp = new \DateTime();
        $hit->save();

        self::updateOrCreateHitStore($hit);
    }

    public function totalHits()
    {
        $total = HitStore::where('model_id', $this->$this->primaryKey)->where('model_type', get_class($this));

        return $total->first() ? $total->first()->total : 0;
    }

    private function updateOrCreateHitStore($hit)
    {
        $existingHitStore = HitStore::where('model_id', $hit->model_id)->where('model_type', $hit->model_type)->first();

        if ($existingHitStore) {
            HitStore::where('model_id', $hit->model_id)->where('model_type', $hit->model_type)->update([
                'total' => $existingHitStore->total + 1,
                'updated_at' => new \DateTime(),
            ]);
        } else {
            $hitStore = new HitStore();
            $hitStore->model_id = $hit->model_id;
            $hitStore->model_type = $hit->model_type;
            $hitStore->total = 1;
            $hitStore->save();
        }
    }
}