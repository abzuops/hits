<?php

namespace Abzudev\Hits;

use Illuminate\Support\ServiceProvider;
use Abzudev\Hits\Commands\RebuildCommand;

class HitsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/hits.php' => config_path('hits.php'),
        ], 'config');

        if (! class_exists('CreateHitsTable')) {
            $this->publishes([
                __DIR__.'/../database/migrations/create_hits_table.php.stub' => database_path('migrations/'.date('Y_m_d_His', time()).'_create_hits_table.php'),
            ], 'migrations');
        }

        if (! class_exists('CreateHitStoreTable')) {
            $this->publishes([
                __DIR__.'/../database/migrations/create_hit_store_table.php.stub' => database_path('migrations/'.date('Y_m_d_His', time()).'_create_hit_store_table.php'),
            ], 'migrations');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/hits.php', 'hits');
    }
}
